# userstyl.es

This project is an attempt at creating a new userstyles sharing platform
aiming to replace the currently leading but slowly dying [userstyles.org](https://userstyles.org/) website.

Current status : **planning** - we're gathering advice/feedback from userstyles developers in order to draft specs.